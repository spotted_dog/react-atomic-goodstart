jest.autoMockOff();

import React from 'react';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';

const ExampleOrganism = require('../src/app/js/components/organisms/ExampleOrganism').default;

describe('ExampleOrganism', () => {
  it('renders two ExampleMolecules', () => {
    const rendered = TestUtils.renderIntoDocument(
      <ExampleOrganism />
    );

    const repos = TestUtils.scryRenderedDOMComponentsWithTag(rendered, 'h1');

    expect(repos.length).toEqual(3);
  });
});

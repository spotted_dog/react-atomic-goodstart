## react-atomic-goodstart

### Notes
This is a base [ReactJS](http://facebook.github.io/react/) project using
HTML5, SASS/CSS3 and JavaScript best practices with atomic design principles.
All code is linted using it's associated linter for HTML, CSS and JavaScript.

### Pre Build
Install GEM needed for SASS linting

```
gem install scss_lint
```

Install needed Node modules

```
npm install
```

###  Build

```
gulp
```

### Run
Start the server

```
gulp server
```

Then bring up the [Home Page](http://localhost:8088/index.html)

### TODO

* Add unit testing with Jest
* Add UI tests (Selinium or other)
